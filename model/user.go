package model

import "time"

type User struct {
	ID                uint      `json:"id" gorm:"primaryKey"`
	Username          string    `json:"username"`
	Email             string    `json:"email"`
	Phone             string    `json:"phone"`
	EncryptedPassword string    `json:"encrypted_password"`
	IsActiveFlag      bool      `json:"is_active_flag"`
	CreatedAt         time.Time `json:"created_at"`
	CreatedBy         uint      `json:"created_by"`
	UpdatedAt         time.Time `json:"updated_at"`
	UpdatedBy         uint      `json:"updated_by"`
}
