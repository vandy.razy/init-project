FROM alpine:3.14

WORKDIR /apps

COPY ./bin/go-example/api /apps/api

EXPOSE 8080
ENTRYPOINT ["/apps/api"]
