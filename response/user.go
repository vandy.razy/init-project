package response

import "gitlab.com/erloom.id/libraries/go/go-example/model"

type GetUserResponse struct {
	BasePaginateResponse
	Users []model.User `json:"users"`
}
