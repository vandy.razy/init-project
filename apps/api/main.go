package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	goexample "gitlab.com/erloom.id/libraries/go/go-example"
	"gitlab.com/erloom.id/libraries/go/go-example/config"
	"gitlab.com/erloom.id/libraries/go/go-example/handler"
	"gitlab.com/erloom.id/libraries/go/go-example/lib/logger"
)

func init() {
	godotenv.Load()
	logger.Init()
}

func main() {
	db, _ := config.NewPG()
	storage := config.NewStorage()
	smtpClient := config.NewSMTPClient()

	backend := goexample.NewBackend(db, storage, &smtpClient)
	handler := handler.NewHandler(&backend)
	loc, _ := time.LoadLocation("Asia/Jakarta")
	time.Local = loc

	router := chi.NewRouter()

	router.Get("/healthz", handler.Healthz)

	router.Group(func(r chi.Router) {
		r.Use(handler.StandardMiddleware)
		r.Use(handler.PanicMiddlewares)

		r.Route("/user", func(r chi.Router) {
			r.Post("/", handler.CreateUser)
			r.Get("/", handler.GetUsers)
			r.Get("/{ID}", handler.GetUserDetail)
			r.Put("/{ID}", handler.UpdateUser)
			r.Delete("/{ID}", handler.DeleteUser)
		})
	})

	server := &http.Server{
		Addr:    "0.0.0.0:8080",
		Handler: router,
	}

	fmt.Println("Running on :8080")

	server.ListenAndServe()
}
