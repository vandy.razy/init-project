package repository

import (
	"context"
	"errors"
	"github.com/stretchr/testify/mock"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
	"gitlab.com/erloom.id/libraries/go/go-example/request"

	"gitlab.com/erloom.id/libraries/go/go-example/lib"
	"gitlab.com/erloom.id/libraries/go/go-example/lib/logger"
	"gorm.io/gorm"
)

type Repository struct {
	db         *lib.Database
	smtpClient *lib.SMTPClient
}
type RepositoryMock struct {
	mock.Mock
}

func NewRepository(db *lib.Database, smtpClient *lib.SMTPClient) Repository {
	return Repository{db: db, smtpClient: smtpClient}
}

type RepositoryPort interface {
	CreateUser(ctx context.Context, user model.User) (model.User, error)
	GetUsers(ctx context.Context, query request.GetUserQuery) ([]model.User, error)
	GetUserTotal(ctx context.Context, query request.GetUserQuery) (uint, error)
	UpdateUser(ctx context.Context, user model.User) (model.User, error)
	GetUserByID(ctx context.Context, ID uint) (model.User, error)
	DeleteUser(ctx context.Context, ID uint) error
	CreateProfile(ctx context.Context, user model.User) (model.User, error)
	GetProfile(ctx context.Context, query request.GetUserQuery) ([]model.User, error)
}

func (repo *Repository) Transaction(ctx context.Context, fn func(context.Context) error) error {
	trx := repo.db.Begin()

	ctx = context.WithValue(ctx, "Trx", &lib.Database{DB: trx})
	if err := fn(ctx); err != nil {
		trx.Rollback()
		return err
	}

	return trx.Commit().Error
}

func (repo *Repository) SendEmail(ctx context.Context, req lib.SMTPRequest) error {
	return repo.smtpClient.Send(ctx, req)
}

func IsNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

func LogError(ctx context.Context, message string, err error) {
	logger.Error(ctx, message, map[string]interface{}{
		"error": err,
		"tags":  []string{"gorm"},
	})
}

func LogWarn(ctx context.Context, message string) {
	logger.Warn(ctx, message, map[string]interface{}{
		"tags": []string{"gorm"},
	})
}
