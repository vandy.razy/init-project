package repository

import (
	"context"
	"fmt"

	"gitlab.com/erloom.id/libraries/go/go-example/lib"
	"gitlab.com/erloom.id/libraries/go/go-example/lib/logger"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
	"gitlab.com/erloom.id/libraries/go/go-example/request"
)

func (repo *Repository) CreateProfile(ctx context.Context, user model.User) (model.User, error) {
	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	err := tx.Create(&user).Error
	if err != nil {
		logger.Error(ctx, "Error CreateProfile", map[string]interface{}{
			"error": err,
			"tags":  []string{"postgres", "user", "repo"},
		})

		return user, err
	}

	return user, nil
}

func (repo *Repository) GetProfile(ctx context.Context, query request.GetUserQuery) ([]model.User, error) {

	var res []model.User
	statement := repo.db.Model(&res).
		Where("is_active_flag = ?", true)

	if query.Search != "" {
		querySearch := fmt.Sprintf("%s%s%s", "%", query.Search, "%")
		statement = statement.Where("name ILIKE ?", querySearch)
	}

	order := query.GetOrderQuery()
	if order != "" {
		statement = statement.Order(order)
	}

	err := statement.Limit(int(query.Limit)).
		Offset(int(query.GetOffset())).
		Find(&res).Error

	if err != nil {
		logger.Error(ctx, "Error GetUser", map[string]interface{}{
			"error": err,
			"tags":  []string{"postgres", "user", "repo"},
		})

		return res, err
	}

	return res, nil
}
