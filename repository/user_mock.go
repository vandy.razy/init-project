package repository

import (
	"context"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
	"gitlab.com/erloom.id/libraries/go/go-example/request"
)

func (repo *RepositoryMock) CreateUser(ctx context.Context, user model.User) (model.User, error) {
	arguments := repo.Mock.Called(ctx, user)

	if arguments.Get(0) == nil {
		return model.User{}, repo.Mock.Called().Error(1)
	} else {
		user := arguments.Get(0).(model.User)
		return user, nil
	}
}

func (repo *RepositoryMock) GetUsers(ctx context.Context, query request.GetUserQuery) ([]model.User, error) {
	args := repo.Called(query)
	return args.Get(0).([]model.User), args.Error(1)
}
func (repo *RepositoryMock) GetUserTotal(ctx context.Context, query request.GetUserQuery) (uint, error) {
	args := repo.Called(query)
	return args.Get(0).(uint), args.Error(1)
}

func (repo *RepositoryMock) UpdateUser(ctx context.Context, user model.User) (model.User, error) {
	arguments := repo.Mock.Called(ctx, user)

	if arguments.Get(0) == nil {
		return model.User{}, repo.Mock.Called().Error(1)
	} else {
		user := arguments.Get(0).(model.User)
		return user, nil
	}
}

func (repo *RepositoryMock) GetUserByID(ctx context.Context, ID uint) (model.User, error) {
	arguments := repo.Mock.Called(ctx, ID)

	if arguments.Get(0) == nil {
		return model.User{}, repo.Mock.Called().Error(1)
	} else {
		user := arguments.Get(0).(model.User)
		return user, nil
	}
}

func (repo *RepositoryMock) DeleteUser(ctx context.Context, ID uint) error {
	arguments := repo.Mock.Called(ctx, ID)

	if arguments.Get(0) == nil {
		return repo.Mock.Called().Error(1)
	} else {
		return nil
	}
}
