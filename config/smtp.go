package config

import (
	"os"

	"gitlab.com/erloom.id/libraries/go/go-example/lib"
)

func NewSMTPClient() lib.SMTPClient {
	return lib.SMTPClient{
		Host:     os.Getenv("SMTP_HOST"),
		Port:     os.Getenv("SMTP_PORT"),
		Password: os.Getenv("SMTP_PASSWORD"),
		Username: os.Getenv("SMTP_USERNAME"),
		Sender:   os.Getenv("SMTP_SENDER"),
	}
}
