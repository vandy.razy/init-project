package goexample

import (
	"gitlab.com/erloom.id/libraries/go/go-example/lib"
	"gitlab.com/erloom.id/libraries/go/go-example/repository"
	"gitlab.com/erloom.id/libraries/go/go-example/storage"
	"gitlab.com/erloom.id/libraries/go/go-example/usecase"
)

type GoExample struct {
	Usecase *usecase.Usecase
}

func NewBackend(db *lib.Database, storage storage.Storage, smtpclient *lib.SMTPClient) GoExample {
	repository := repository.NewRepository(db, smtpclient)
	usecase := usecase.NewUsecase(&repository, storage)

	return GoExample{
		Usecase: &usecase,
	}
}
