package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"gitlab.com/erloom.id/libraries/go/go-example/lib"
	"gitlab.com/erloom.id/libraries/go/go-example/request"
)

func (handler *Handler) CreateUser(rw http.ResponseWriter, r *http.Request) {
	var payload request.CreateUserRequest

	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	err = validator.New().Struct(payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	res, err := handler.Backend.Usecase.CreateUser(r.Context(), payload)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) GetUsers(rw http.ResponseWriter, r *http.Request) {
	queryParams := r.URL.Query()

	var limit, page int

	limit, _ = strconv.Atoi(queryParams.Get("limit"))
	page, _ = strconv.Atoi(queryParams.Get("page"))

	sort := strings.Split(queryParams.Get("sort"), ",")

	var query request.GetUserQuery

	query.Limit = uint(limit)
	query.Page = uint(page)
	query.Search = queryParams.Get("search")
	query.Sort = sort

	res, err := handler.Backend.Usecase.GetUsers(r.Context(), query)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) GetUserDetail(rw http.ResponseWriter, r *http.Request) {
	idString := chi.URLParam(r, "ID")
	id, _ := strconv.ParseUint(idString, 10, 32)

	res, err := handler.Backend.Usecase.GetUserDetail(r.Context(), uint(id))
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) UpdateUser(rw http.ResponseWriter, r *http.Request) {
	var payload request.UpdateUserRequest

	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	err = validator.New().Struct(payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	idString := chi.URLParam(r, "ID")
	id, _ := strconv.ParseUint(idString, 10, 32)

	payload.UserID = uint(id)

	res, err := handler.Backend.Usecase.UpdateUser(r.Context(), payload)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) DeleteUser(rw http.ResponseWriter, r *http.Request) {
	idString := chi.URLParam(r, "ID")
	id, _ := strconv.ParseUint(idString, 10, 32)

	err := handler.Backend.Usecase.DeleteUser(r.Context(), uint(id))
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, nil, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}
