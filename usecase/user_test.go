package usecase

import (
	"context"
	"testing"

	"errors"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/erloom.id/libraries/go/go-example/config"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
	"gitlab.com/erloom.id/libraries/go/go-example/repository"
	"gitlab.com/erloom.id/libraries/go/go-example/request"
)

var strg = config.NewLocalStorage()
var userRepo = &repository.RepositoryMock{}
var usc = NewUsecase(userRepo, strg)

func TestGetUsers(t *testing.T) {

	ctx := context.Background()
	query := request.GetUserQuery{}

	Convey("GetUsers Usecase Scenario", t, func() {
		Convey("Success", func() {
			userRepo.On("GetUsers", query).Return([]model.User{}, nil).Once()
			userRepo.On("GetUserTotal", query).Return(uint(1), nil).Once()

			res, err := usc.GetUsers(ctx, query)
			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)
		})

		Convey("Fail", func() {
			//var res []model.User
			userRepo.On("GetUsers", query).Return([]model.User{}, errors.New("Error GetUsers")).Once()
			userRepo.On("GetUserTotal", query).Return(uint(0), errors.New("Error GetUserTotal")).Once()

			res, err := usc.GetUsers(ctx, query)
			So(res, ShouldNotBeNil)
			So(err, ShouldNotBeNil)
		})
	})
}
