package usecase

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/erloom.id/libraries/go/go-example/lib/logger"

	"gitlab.com/erloom.id/libraries/go/go-example/lib"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
	"gitlab.com/erloom.id/libraries/go/go-example/request"
	"gitlab.com/erloom.id/libraries/go/go-example/response"
)

func (usecase *Usecase) CreateUser(ctx context.Context, payload request.CreateUserRequest) (model.User, error) {
	var user model.User

	encryptedPassword, err := lib.GenerateHashFromString(payload.Password)
	if err != nil {
		logger.Error(ctx, "Error EncryptPassword", map[string]interface{}{
			"error": err,
			"tags":  []string{"auth", "login"},
		})

		return user, lib.ErrorInternalServer
	}

	user = model.User{
		Username:          payload.Username,
		Email:             payload.Email,
		Phone:             payload.Phone,
		EncryptedPassword: encryptedPassword,
		IsActiveFlag:      true,
		CreatedAt:         time.Now(),
		CreatedBy:         payload.CreatedBy,
		UpdatedAt:         time.Now(),
		UpdatedBy:         payload.CreatedBy,
	}

	res, err := usecase.repo.CreateUser(ctx, user)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (usecase *Usecase) GetUsers(ctx context.Context, query request.GetUserQuery) (response.GetUserResponse, error) {
	var res response.GetUserResponse
	var err error

	users, err := usecase.repo.GetUsers(ctx, query)
	if err != nil {
		return res, err
	}

	total, err := usecase.repo.GetUserTotal(ctx, query)
	fmt.Println(err)
	if err != nil {
		return res, err
	}

	res.Users = users
	res.Total = total
	res.Limit = query.Limit
	res.Offset = query.GetOffset()
	res.Page = query.Page

	return res, nil
}

func (usecase *Usecase) GetUserDetail(ctx context.Context, ID uint) (model.User, error) {
	var user model.User

	user, err := usecase.repo.GetUserByID(ctx, ID)
	if err != nil {
		return user, err
	}

	return user, nil
}

func (usecase *Usecase) UpdateUser(ctx context.Context, payload request.UpdateUserRequest) (model.User, error) {
	var user model.User

	user, err := usecase.repo.GetUserByID(ctx, payload.UserID)
	if err != nil {
		return user, err
	}

	encryptedPassword, err := lib.GenerateHashFromString(payload.Password)
	if err != nil {
		logger.Error(ctx, "Error EncryptPassword", map[string]interface{}{
			"error": err,
			"tags":  []string{"auth", "login"},
		})

		return user, lib.ErrorInternalServer
	}

	user.Username = payload.Username
	user.Email = payload.Email
	user.Phone = payload.Phone
	user.EncryptedPassword = encryptedPassword
	user.UpdatedAt = time.Now()
	user.UpdatedBy = payload.UpdatedBy

	res, err := usecase.repo.UpdateUser(ctx, user)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (usecase *Usecase) DeleteUser(ctx context.Context, ID uint) error {
	err := usecase.repo.DeleteUser(ctx, ID)
	if err != nil {
		return err
	}

	return nil
}
