package usecase

import (
	"context"

	"gitlab.com/erloom.id/libraries/go/go-example/lib/logger"
	"gitlab.com/erloom.id/libraries/go/go-example/repository"
	"gitlab.com/erloom.id/libraries/go/go-example/storage"
)

type Usecase struct {
	repo    repository.RepositoryPort
	storage storage.Storage
}

func NewUsecase(repo repository.RepositoryPort, storage storage.Storage) Usecase {
	return Usecase{repo: repo, storage: storage}
}

func LogError(ctx context.Context, message string, err error) {
	logger.Error(ctx, message, map[string]interface{}{
		"error": err,
		"tags":  []string{"gorm"},
	})
}
