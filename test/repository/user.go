package repository

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
)

type UserRepository interface {
	FindByID(ID int) *model.User
}

type UserRepositoryMock struct {
	Mock mock.Mock
}

func (repo *UserRepositoryMock) FindByID(ID int) *model.User {
	arguments := repo.Mock.Called(ID)

	if arguments.Get(0) == nil {
		return nil
	} else {
		user := arguments.Get(0).(model.User)
		return &user
	}
}
