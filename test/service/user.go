package service

import (
	"errors"

	"gitlab.com/erloom.id/libraries/go/go-example/model"
	"gitlab.com/erloom.id/libraries/go/go-example/test/repository"
)

type UserService struct {
	Repository repository.UserRepository
}

func (service UserService) Gets(ID int) (*model.User, error) {
	user := service.Repository.FindByID(ID)

	if user == nil {
		return nil, errors.New("User Not Found")
	} else {
		return user, nil
	}
}

// func (service UserService) GetUsers(query request.GetUserQuery) ([]model.User, error) {
// 	user := service.Repository.FindByID(ID)

// 	if user == nil {
// 		return nil, errors.New("User Not Found")
// 	} else {
// 		return user, nil
// 	}
// }
