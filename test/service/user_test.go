package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/erloom.id/libraries/go/go-example/model"
	repo "gitlab.com/erloom.id/libraries/go/go-example/repository"
	"gitlab.com/erloom.id/libraries/go/go-example/test/repository"
)

var userRepository = &repository.UserRepositoryMock{Mock: mock.Mock{}}
var userService = UserService{Repository: userRepository}

func TestUserService_Get(t *testing.T) {
	user := model.User{
		ID:       1,
		Username: "My Test",
		Email:    "my@test.com",
	}

	userRepository.Mock.On("FindByID", 1).Return(user)

	res, err := userService.Gets(1)

	assert.Nil(t, err)
	assert.NotNil(t, res)
}

var userRepository2 = &repo.RepositoryMock{Mock: mock.Mock{}}
var userService2 = UserService{Repository: userRepository}
