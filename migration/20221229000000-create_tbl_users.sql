-- +migrate Up
CREATE TABLE IF NOT EXISTS users(
    id SERIAL NOT NULL PRIMARY KEY,
    username VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(100),
    encrypted_password VARCHAR(255) NOT NULL,
    is_active_flag BOOLEAN,
    created_at TIMESTAMP NOT NULL,
    created_by INT,
    updated_at TIMESTAMP NOT NULL,
    updated_by INT
);

-- +migrate Down
DROP TABLE IF EXISTS users;